Custom USB Keyboard
===================

I built my own keyboard following the example set by Dave Cooper in [My First
Keyboard Build](http://www.davecooper.org/i-built-a-keyboard). It's an 83-Key
mechanical keyboard with a custom layout and is driven by a [Teensy
2.0](https://www.pjrc.com/store/teensy.html).

[The photos](https://goo.gl/photos/pDqyw6ashz7WeetJA) show each step of the
hardware build and the final product.

This repository includes:

- Schematics for the keyboard plates (the top plate with cutouts for
  mounting switches and the bottom plate that acts as the base) and a frame.
- USB library for the Teensy 2.0: [USB Keyboard, Version
  1.1](https://www.pjrc.com/teensy/usb_keyboard.html).
- Key mapping and driver code in `keyboard.c`.

The Hardware
------------

- Teensy 2.0 was purchased from [Ebay](https://www.ebay.com.au).
- Cherry MX Brown Keyswitches (Plate Mounted), Keycap set and stabilizers were
  purchased from [WASD Keyboards](https://www.wasdkeyboards.com).
- 1N4148 diodes, hook-up wire and solder were purchased from
  [Jaycar](http://www.jaycar.com.au).
- The 1.6mm stainless steel top plate, 2mm stainless steel bottom plate and
  the 15mm clear Acrylic frame was laser cut by
  [Lasermade](http://www.lasermade.com.au).

I more or less followed the instructions in [My First Keyboard
Build](http://www.davecooper.org/i-built-a-keyboard) to setup the hardware.
This is easily the most time-consuming and enjoyable part of the project.
Frankly, I started this project so that I could spend a lot of time soldering.

The Software
------------

To build the keyboard driver:
1. Clone this repository.
2. Build with the following command:

        make all

To load the software onto the device:
1. Download the [Teensy Loader
   Application](https://www.pjrc.com/teensy/loader.html) for your OS.
2. Follow the instructions to load the `keyboard.hex` file onto the Teensy.

To customize the key mapping, open `keyboard.c` and modify `KEY_MAP` to your
liking then rebuild.
