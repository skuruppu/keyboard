/* USB keyboard controller software
 * Copyright (C) 2015 Shanika Kuruppu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "usb_keyboard.h"

// CLKPR = 0x80 tells Atmega that we want to change the system clock.
// CLKPR = (n) scales the 16 MHz clock by 1/(2^n).
#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))

// Number of rows in the keyboard matrix.
#define ROWS 6

// Number of columns in the keyboard matrix.
#define COLUMNS 16

// Maps row numbers to Port F pins.
uint8_t ROW_TO_PIN[ROWS] = {0, 1, 4, 5, 6, 7};

void select_row(uint8_t row) {
    // Unselect all rows if row is invalid.
    if (row >= ROWS) {
        DDRF = 0x00;
        return;
    }
    DDRF = 0x01 << ROW_TO_PIN[row];
}

// Maps column numbers to the port where their pins are found. 0 for Port B
// and 1 for Port D.
uint8_t COLUMN_TO_PORT[COLUMNS] = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                                   0, 0};

// Maps column numbers to a pin of the port COLUMN_TO_PORT[column].
uint8_t COLUMN_TO_PIN[COLUMNS] = {0, 1, 2, 3, 7, 0, 1, 2, 3, 5, 4, 6, 7, 4, 5,
                                  6};

uint8_t is_column_selected(uint8_t pin_b, uint8_t pin_d, uint8_t column) {
    if (column >= COLUMNS) {
        return 0;
    }

    uint8_t pin;
    if (COLUMN_TO_PORT[column]) {
        pin = pin_d;
    } else {
        pin = pin_b;
    }

    // Returns true if the pin is low.
    return (pin & (0x01 << COLUMN_TO_PIN[column])) == 0;
}

// A modifier key (shift, ctrl, alt and GUI).
#define MOD_KEY 0x01

// Maps row and column entries in the keyboard matrix to keys. 0x00 indicates
// that there is no key in that entry. MOD_KEY indicates that the entry
// corresponds to a modifier key. The MODIFIER_KEY_MAP should be looked up to
// get the corresponding modifier key.
uint8_t KEY_MAP[ROWS][COLUMNS] = {
    // Row A
    {
        KEY_ESC, KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7,
        KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12, KEY_PRINTSCREEN,
        KEY_SCROLL_LOCK, KEY_PAUSE
    },
    // Row B
    {
        KEY_TILDE, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8,
        KEY_9, KEY_0, KEY_MINUS, KEY_EQUAL, KEY_BACKSPACE, 0x00, KEY_DELETE
    },
    // Row C
    {
        KEY_TAB, KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T, KEY_Y, KEY_U, KEY_I,
        KEY_O, KEY_P, KEY_LEFT_BRACE, KEY_RIGHT_BRACE, KEY_BACKSLASH, 0x00,
        KEY_INSERT
    },
    // Row D
    {
        KEY_CAPS_LOCK, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H, KEY_J, KEY_K,
        KEY_L, KEY_SEMICOLON, KEY_QUOTE, 0x00, KEY_ENTER, 0x00, KEY_PAGE_UP
    },
    // Row E
    {
        MOD_KEY, KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N, KEY_M, KEY_COMMA,
        KEY_PERIOD, KEY_SLASH, 0x00, MOD_KEY, 0x00, KEY_UP, KEY_PAGE_DOWN
    },
    // Row F
    {
        MOD_KEY, MOD_KEY, MOD_KEY, 0x00, 0x00, KEY_SPACE, 0x00, 0x00, 0x00,
        KEY_HOME, KEY_END, 0x00, KEY_LEFT, 0x00, KEY_DOWN, KEY_RIGHT
    }
};

// Map of modifier keys in the keyboard matrix. 0x00 indicates that there is
// no modifier key in that entry.
uint8_t MODIFIER_KEY_MAP[ROWS][COLUMNS] = {
    // Row A
    {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    },
    // Row B
    {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    },
    // Row C
    {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    },
    // Row D
    {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00
    },
    // Row E
    {
        KEY_LEFT_SHIFT, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, KEY_RIGHT_SHIFT, 0x00, 0x00, 0x00
    },
    // Row F
    {
        KEY_LEFT_CTRL, KEY_LEFT_GUI, KEY_LEFT_ALT, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    }
};

// The number of keys that can be pressed simultaneously excluding modifier
// keys.
#define SIMULTANEOUS_KEYS 6

void reset_key_state(void) {
    for (uint8_t i = 0; i < SIMULTANEOUS_KEYS; i++) {
        keyboard_keys[i] = 0;
    }
    keyboard_modifier_keys = 0;
}

// Determines the number of iterations for which the keyboard has been idle.
// It has three states. PRESSED = keys are currently pressed, DEPRESSED = the
// first iteration of idleness after some key presses, IDLE = keyboard has
// been idle for two or more iterations so don't bother to send the keyboard
// state.
typedef enum {PRESSED, DEPRESSED, IDLE} keyboard_state;

int main(void) {
    // Don't prescale and leave the clock at 16 MHz.
    CPU_PRESCALE(0);

    // See the "Using I/O Pins" page for details
    // (http://www.pjrc.com/teensy/pins.html).

    // Port F pins are used as rows. They are configured as "open collector"
    // outputs where DDRF of a pin is set to 1 to register a 0 output and it
    // is set to 0 to allow the pin to float.
    PORTF = 0x00;
    // Port B and D pins are used as columns. They are configured as inputs
    // with pullup resistors.
    DDRB = 0x00;
    PORTB = 0xFF;
    DDRD = 0x00;
    PORTD = 0xFF;

    // Initialize the USB, and then wait for the host to set configuration.
    // If the Teensy is powered without a PC connected to the USB port, this
    // will wait forever.
    usb_init();
    while (!usb_configured()) /* wait */;

    // Wait an extra second for the PC's operating system to load drivers
    // and do whatever it does to actually be ready for input.
    _delay_ms(1000);

    uint8_t key_pressed, keys_pressed;
    // Keeps track of the keyboard idleness state.
    keyboard_state state = IDLE;
    while(1) {
        // Reset the key state before setting the next round of key presses.
        reset_key_state();
        keys_pressed = 0;

        for (uint8_t row = 0; row < ROWS; row++) {
            // Each time around the loop, register a 0 output for the current
            // row which will set a 1 in any columns where a key is pressed in
            // that row. In the first iteration, port F0 is set, in the
            // second, F1 is set, in the third, F2 is set, and so on. After
            // F5, it cycles back to port F0.
            select_row(row);

            // Leave enough time for propagation and time between
            // usb_keyboard_send calls.
            _delay_ms(5);

            // Check which columns are selected.
            uint8_t pin_b = PINB;
            uint8_t pin_d = PIND;
            for (uint8_t column = 0; column < COLUMNS; column++) {
                if (!is_column_selected(pin_b, pin_d, column)) {
                    continue;
                }

                key_pressed = KEY_MAP[row][column];
                // A cell in the matrix that's not mapped to a key.
                if (!key_pressed) {
                    continue;
                }
                if (key_pressed == MOD_KEY) {
                    keyboard_modifier_keys |= MODIFIER_KEY_MAP[row][column];
                    continue;
                }
                // Only add the key to the key state if the key state is not
                // full.
                if (keys_pressed < SIMULTANEOUS_KEYS) {
                    keyboard_keys[keys_pressed] = key_pressed;
                    keys_pressed ++;
                }
            }
        }

        if (keys_pressed || keyboard_modifier_keys) {
            state = PRESSED;
        } else if (state == PRESSED) {
            state = DEPRESSED;
        } else {
            state = IDLE;
        }

        // Send the key state if keys are currently being pressed or if we
        // need to send the key up state for the last round of key presses.
        if (state <= DEPRESSED) {
            usb_keyboard_send();
        }
    }

    return 0;
}
